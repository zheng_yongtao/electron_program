async function getImg() {
  try {
    Toast.showLoading("正在获取图片");
    const imageFiles = await gitOperate.getImg();
    if(imageFiles.length  === 0){
      Toast.hide();
      Toast.showToast('暂无图片');
      return;
    }
    waterfall.create(imageFiles, Toast.hide);
  } catch (err) {
    Toast.hide();
    Toast.showToast(err);
  }
}
function menuClick(type) {
  const clickItem = mouseMenu.clickItem;
  const sha = clickItem.getAttribute("data-sha");
  const src = clickItem.src;
  let imgName = src.split("/");
  imgName = imgName[imgName.length - 1];
  switch (type) {
    case "删除":
      gitOperate.deleteImg(imgName, sha, getImg);
      break;
    case "复制图片链接":
      document.body.focus();
      copyToClipboard(src);
      break;
    case "复制为Markdown链接":
      const url = src.split('/');
      const link = `![${url[url.length - 1]}](${src})`
      document.body.focus();
      copyToClipboard(link);
      break;
    }
}
