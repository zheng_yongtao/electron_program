class MouseMenu {
  constructor(config) {
    this.menuClass = config.menuClass || "j-mouse-menu";
    this.menuId = config.menuId || "JMouseMenu";
    this.contentId = config.contentId || "j-mouse-content";
    this.init();
  }
  init() {
    const dom = document.getElementById(this.contentId);
    dom.oncontextmenu = (e) => {
      const clickItem = e.path[0];
      if (clickItem.localName !== "img") return;
      const menu = document.getElementById(this.menuId);
      this.clickItem = clickItem;
      this.hideAllMenu();
      // 自定义body元素的鼠标事件处理函数
      e = e || window.event;
      e.preventDefault();
      let scrollTop =
        document.documentElement.scrollTop || document.body.scrollTop; // 获取垂直滚动条位置
      let scrollLeft =
        document.documentElement.scrollLeft || document.body.scrollLeft; // 获取水平滚动条位置
      menu.style.display = "block";
      let left = e.clientX + scrollLeft;
      let top = e.clientY + scrollTop;
      if (menu.offsetHeight + top > window.innerHeight) {
        top = window.innerHeight - menu.offsetHeight - 5;
      }
      if (menu.offsetWidth + left > window.innerWidth) {
        left = window.innerWidth - menu.offsetWidth - 5;
      }
      menu.style.left = left + "px";
      menu.style.top = top + "px";
      document.onclick = () => {
        this.hideAllMenu();
      };
    };
  }
  hideAllMenu() {
    const jMenu = document.getElementsByClassName("j-mouse-menu");
    for (const item of jMenu) {
      item.style.display = "none";
    }
  }
}
