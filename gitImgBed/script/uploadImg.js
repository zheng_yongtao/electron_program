const dropzone = document.getElementById("dropzone");
const previewUrl = document.getElementById("previewUrl");
const previewForMarkdown = document.getElementById("previewForMarkdown");

const uploadBtn = document.getElementById("upload-btn");
let file;
function dragStyle(e) {
  e.preventDefault();
  dropzone.style.border = "2px dashed #aaa";
}
dropzone.addEventListener("dragover", dragStyle);

dropzone.addEventListener("dragleave", dragStyle);

dropzone.addEventListener("drop", (e) => {
  dragStyle(e);
  file = e.dataTransfer.files[0];
  showPreview(file);
});

dropzone.addEventListener("click", () => {
  const input = document.createElement("input");
  input.type = "file";
  input.accept = "image/*";
  input.onchange = (e) => {
    showPreview(e.target.files[0]);
  };
  input.click();
});

uploadBtn.addEventListener("click", () => {
  if (!file) {
    alert("请先选择文件！");
    return;
  }

  const reader = new FileReader();

  reader.onload = async () => {
    const base64Data = reader.result.split(",")[1];
    const imageUrl = await gitOperate.uploadToGitee(base64Data);
    previewUrl.value = imageUrl;
    const url = imageUrl.split('/');
    const link = `![${url[url.length - 1]}](${imageUrl})`
    previewForMarkdown.value = link;
    Toast.showToast("已上传");
    waterfall.init();
  };
  reader.readAsDataURL(file);
});

function showPreview(fileTmp) {
  file = fileTmp;
  previewUrl.value = "";
  previewForMarkdown.value = "";
  const reader = new FileReader();

  reader.onload = () => {
    const img = document.createElement("img");
    img.src = reader.result;
    img.style.maxWidth = "100%";
    img.style.maxHeight = "100%";
    img.style.margin = "auto";
    dropzone.innerHTML = "";
    dropzone.appendChild(img);
  };

  reader.readAsDataURL(file);
}
document.addEventListener("paste", function (e) {
  const items = e.clipboardData.items;

  for (const item of items) {
    if (item.type.indexOf("image") !== -1) {
      const blob = item.getAsFile();
      showPreview(blob);
    }
  }
});
function blobToBase64(blob) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = () => {
      const base64String = reader.result.split(",")[1];
      resolve(base64String);
    };
    reader.onerror = reject;
    reader.readAsDataURL(blob);
  });
}
function selectFolder() {
  const folderInput = document.getElementById("folderInput");
  folderInput.click();
}

async function uploadImages() {
  const folderInput = document.getElementById("folderInput");
  let files = folderInput.files || [];
  files = [...files].filter((file) => file.type.startsWith("image/"));

  for (let i = 0; i < files.length; i++) {
    Toast.showLoading(`上传中，${i}/${files.length}`);
    const file = files[i];
    const base64Data = await blobToBase64(file);
    await gitOperate.uploadToGitee(base64Data,false);
  }
  Toast.hide();
  Toast.showToast(`已全部上传`);
  waterfall.init();
}

function urlCopy(id){
  const value = document.getElementById(id).value;
  if(!value){
    Toast.showToast('请先上传图片');
    return;
  }
  copyToClipboard(value);
}
