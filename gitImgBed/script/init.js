const gitConfig = JSON.parse(localStorage.getItem("gitConfig") || "{}");
for (const key in gitConfig) {
  document.getElementById(key).value = gitConfig[key];
}
let gitOperate = new GitOperate(gitConfig);
const Toast = new ToastC();
const mouseMenu = new MouseMenu({
  menuClass: "j-mouse-menu",
  menuId: "JMouseMenu",
  contentId: "waterfall-container",
});
const waterfall = new WaterfallContent();
function copyToClipboard(text) {
  navigator.clipboard
    .writeText(text)
    .then(() => {
      Toast.showToast("内容已成功复制到剪贴板");
    })
    .catch((error) => {
      Toast.showToast("无法复制内容到剪贴板:" + error);
    });
}
