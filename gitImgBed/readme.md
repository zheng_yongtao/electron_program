## 常见问题
### 1、打包报错
- 问题描述
> Packaging app for platform win32 x64 using electron v13.6.9 Command failed with a non-zero return code (1): E:\myGitee\electron_program\gitImgBed\node_modules\rcedit\bin\rcedit-x64.exe C:\Users\MT-USER\AppData\Local\Temp\electron

- 解决方法
> 这个问题可能是由于在 Windows 系统上使用 electron-packager 打包应用时，rcedit-x64.exe 工具无法正常工作所导致的。为了解决这个问题，你可以尝试以下解决方法：

1. 更新 electron-packager：首先尝试更新 electron-packager 到最新版本，以确保它与当前运行的 Electron 版本兼容。可以在命令行中运行以下命令进行更新：

```shell
npm install -g electron-packager
```

2. 安装 Visual C++ Redistributable：如果电脑上尚未安装 Visual C++ Redistributable 2015，请到 Microsoft 官网下载并安装。下载地址：https://www.microsoft.com/en-us/download/details.aspx?id=48145

3. 重新打包应用：完成上述步骤后，尝试重新运行 electron-packager 命令进行打包。

如果以上方法仍然不能解决问题，可以尝试手动安装 rcedit，并指定其路径。具体步骤如下：

1. 手动安装 rcedit：在命令行中运行以下命令手动安装 rcedit：

```shell
npm install rcedit
```

2. 指定 rcedit 路径：在运行 electron-packager 命令时，指定 rcedit 的路径，例如：

```shell
electron-packager . myApp --platform=win32 --arch=x64 --overwrite --icon=./icon.ico --prune --asar --rcedit=E:\path\to\rcedit-x64.exe
```

将 `E:\path\to\rcedit-x64.exe` 替换为你的 rcedit 实际路径。

希望这些方法能够帮助你解决问题。如果你有任何其他问题，请随时提问。